reset
rp="0.7"
set terminal svg size 600,175 dynamic enhanced fname 'arial'  fsize 18 butt dashed solid
set object 1 rect from screen 0, 0, 0 to screen 1, 1, 0 behind
set object 1 rect fc  rgb "white"  fillstyle solid 1.0
set output "IONS_equ.svg"
set xlabel ""
set ylabel "5' - 1KF1 - 3'\n 4 3 2 1"
set yr [0:5]
unset ytics
set xtics nomirror
set border 15 lw 1

set palette negative defined ( \
    0 '#D53E4F',\
    1 '#F46D43',\
    2 '#FDAE61',\
    3 '#FEE08B',\
    4 '#E6F598',\
    5 '#ABDDA4',\
    6 '#66C2A5',\
    7 '#3288BD' )

p 'out.txt_01.txt' u ($5/1000):($1+1.25):6 every 20 w p ps rp pt 5 palette notitle,\
'out.txt_01.txt' u ($5/1000):($2+1.25):7 every 20 w p ps rp pt 5 palette notitle,\
'out.txt_01.txt' u ($5/1000):($3+1.25):8 every 20 w p ps rp pt 5 palette notitle,\
'out.txt_01.txt' u ($5/1000):($4+1.25):9 every 20 w p ps rp pt 5 palette notitle

set output
set term x11
