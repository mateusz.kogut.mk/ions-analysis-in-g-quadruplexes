#!/bin/bash
#WERSJA 28.09.14
shopt -s -o nounset

USAGE="\nARGUMENTY:\n\n-xtc\ttraj.xtc\n-tpr\ttopol.trp\n-ndx\tindex.ndx\n-out\tout.txt\n-ion\tgrupa zawierajaca analizowane jony\n-gr\tgrupy zawierajace atomy wyznaczajace rozwazane plaszczyzny w formacie: \"11 12 13\" - wazna jest kolejnosc i cudzyslow\n-dist\tkryterium - zalecane  wieksze niz polowa odleglosci miedzy plaszczyznami i mniejsze niz ta odleglosc\n"

echo -e $USAGE

OUT=out

if [[ ! -e ./c.py ]]; then
    echo -e "Nie znalezniono pliku c.py!\n"
    exit;
fi

if [ $# -eq 0 ]; then
   echo -e "Podaj argumenty!\n"
   exit;
fi

while [ $# -gt 0 ]; do
   case  "$1" in
      -tpr)  shift
             TPR=$1
		;;
      -ndx)  shift
             NDX=$1
		;;
      -xtc)  shift
             XTC=$1
		;;
      -dist) shift
             DIST=$1
		;;
      -out)  shift
             OUT=$1
		;;
      -ion)  shift
             IONS=$1
		;;
      -gr)   shift
             PLANE=(${1})
		;;
      *)     echo "Niewlasciwy argument $1"
             exit
		;;
   esac
   shift
done

#IONS=2				#grupa w ndx zawierajaca jony
#PLANE=(11 12 13)		#grupy w ndx zawierajace atomy z plaszczyzn
#TPR="dyn_2JPZ.tpr"		#plik tpr
#NDX="index.ndx"		#plik indeksowy
#XTC="dyn_2JPZ.part0074.xtc"	#trajektoria
#DIST=0.165			#kryterium

echo 'Analizowanie trajektorii...'

gmxcheck -f $XTC &> tmptime.txt
start_t=$(cat tmptime.txt | grep " 0 time" | awk '{print $5}')
steps=$(cat tmptime.txt | grep "^Step" | awk '{print $2}')
timestep=$(cat tmptime.txt | grep "^Step" | awk '{print $3}')
end_t=$(echo "$start_t + $steps * $timestep - $timestep" | bc)

echo 'Obliczanie odleglosci...'

for i in ${PLANE[@]};
do
   #echo $i $IONS | g_dist -f $XTC -s $TPR -n $NDX -dist $DIST > tmp"$i".txt;
   #cat tmp"$i".txt | grep -v "Selected" | sed -e 's///g;s/t\://g' | awk '{print $1, $4}' > plane"$i".txt;
   gmx select -f $XTC -s $TPR -n $NDX -select "group $IONS and within $DIST of com of group $i" -oi index"$i".dat; 
   cat index"$i".dat | grep "000    1" | sed 's/\.000    1//g' | awk '{print $1,$2}' > plane_tmp;
   cat index"$i".dat | grep "000    2" | sed 's/\.000    2//g' | awk '{print $1,$2}' >> plane_tmp;
   cat index"$i".dat | grep "000    2" | sed 's/\.000    2//g' | awk '{print $1,$3}' >> plane_tmp
   cat plane_tmp | sort -n > plane"$i".txt;
   rm plane_tmp
done

./c.py $start_t $end_t $timestep $steps $(for i in ${PLANE[@]}; do echo plane"$i".txt; done) 

for i in $(seq $start_t $timestep $end_t);do echo $(seq 0 1 $(ls plane* | wc -l)) >> ttt ;done

cat ions.txt | sed s/\'//g | sed s/\,//g | sed 's/\[//g' | sed 's/\]//g' > ttt2
paste ttt ttt2 > $OUT'.txt'
cat ions_01.txt | sed s/\'//g | sed s/\,//g | sed 's/\[//g' | sed 's/\]//g' > ttt3
paste ttt ttt3 > $OUT'_01.txt'
rm ions.txt ions_01.txt ttt*
mv ions_info.txt $OUT'_info.txt'

