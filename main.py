#!/usr/bin/env python
#WERSJA 03.10.14
#dodano mozliwosc zdefiniowania dowolnej ilosci plaszczyzn
import sys
 
print 'Wczytywanie...'

#wczytywanie plikow z g_dist i tworzenie z nich macierzy, dodawanie brakujacych czasow

def func(x,start_time,end_time,dt):
	plik = open(x).readlines()
	matrix = []
	for i in plik:
	        i = i.rstrip("\n")
	        i = i.split(' ',2)
	        matrix.append(i)
	print matrix
	time = start_time
	out = [[time]]
	ii = 0
	time_index = 0
	while time <= end_time:
	        if time != out[-1][0]:
	               out.append([time])
#	        print matrix[ii]
#	        print matrix[ii][0], "time",time
	        if ii != len(matrix):
	                if float(matrix[ii][0]) == time:
	                        out[time_index].extend(matrix[ii][1:])
	                        ii += 1
	                else:
	                        time += dt
				time_index += 1
	        else:
	                time += dt
			time_index += 1
#	        if len(out[time_index-1]) == 1:
#	                out[time_index-1].extend([0])
#	        print "out" , out
	return out
start_t = float(sys.argv[1])
end_t = float(sys.argv[2])
timestep = float(sys.argv[3])
steps = float(sys.argv[4])
files = []
for i in range(5, len(sys.argv)):
	files.append(str(sys.argv[i]))
#	print files

#print globals()
#print file1 , file2 , file3 , start_t , end_t , timestep, steps

#listy jonow w kontakcie z poszczegolnymi plaszczyznami

for i in files:
	globals()[i] = func(i, start_t, end_t, timestep)	

#for i in globals()[files[0]]:
#	print i
#for i in globals()[files[1]]:
#	print i




if all(len(x) == 0 for x in files):
	print 'ERROR' + '\n' + 'Brak jonow spelniajacych kryterium'
        sys.exit(0)


#if len(plane1) == 0 and len(plane2) == 0 and len(plane3) == 0:
#	print 'ERROR' + '\n' + 'Brak jonow spelniajacych kryterium'
#	sys.exit(0)
	
print 'Tworzenie listy obsadzenia poszczegolnych pozycji...'

#analiza powyzszych list, tworzenie pliku z obsadzeniami poszczegolnych pozycji w poszczegolnych klatkach

ions = []
for n in range(0, len(globals()[files[0]])):
	
#	print 'ta czesc sprawdza pozycje nad pierwsza plaszczyzna'

	ions.append([globals()[files[0]][n][0]])
	if len(globals()[files[0]][n]) == 1:
		ions[n].append('0')
	elif len(globals()[files[0]][n]) == 2:
		if globals()[files[0]][n][1] not in globals()[files[1]][n]:
                        ions[n].append(globals()[files[0]][n][1])
		else:
                        ions[n].append('0')
	elif len(globals()[files[0]][n]) == 3:
		if globals()[files[0]][n][1] not in globals()[files[1]][n]:
			ions[n].append(globals()[files[0]][n][1])
		elif globals()[files[0]][n][2] not in globals()[files[1]][n]:
			ions[n].append(globals()[files[0]][n][2])
		else:
			ions[n].append('0')
	else:
		print 'ERROR 1'
                sys.exit(0)

#	print 'ta czesc sprawdza posrednie pozycje'

	for m in range(1,len(files)):
		if len(globals()[files[m]][n]) == 1:
        	        ions[n].append('0')
		elif len(globals()[files[m]][n]) == 2:
        	        if globals()[files[m]][n][1] in globals()[files[m-1]][n]:
                	        ions[n].append(globals()[files[m]][n][1])
        	        else:
                	        ions[n].append('0')
      		elif len(globals()[files[m]][n]) == 3:
                	if globals()[files[m]][n][1] in globals()[files[m-1]][n]:
                        	ions[n].append(globals()[files[m]][n][1])
                	elif globals()[files[m]][n][2] in globals()[files[m-1]][n]:
                        	ions[n].append(globals()[files[m]][n][2])
               		else:
                        	ions[n].append('0')
       		else:
                	print 'ERROR 2'
                	sys.exit(0)

#	print 'ta czesc sprawdza pozycje pod ostatnia plaszczyzna'

	if len(globals()[files[-1]][n]) == 1:
                ions[n].append('0')
        elif len(globals()[files[-1]][n]) == 2:
                if globals()[files[-1]][n][1] not in globals()[files[-2]][n]:
                        ions[n].append(globals()[files[-1]][n][1])
                else:
                        ions[n].append('0')
        elif len(globals()[files[-1]][n]) == 3:
                if globals()[files[-1]][n][1] not in globals()[files[-2]][n]:
                        ions[n].append(globals()[files[-1]][n][1])
                elif globals()[files[-1]][n][2] not in globals()[files[-2]][n]:
                        ions[n].append(globals()[files[-1]][n][2])
                else:
                        ions[n].append('0')
        else:
                print 'ERROR 3'
                sys.exit(0)

#Works as intended	
######################################################################################	
				

ionsfile = open('ions.txt', 'w')
for x in ions:
	ionsfile.write(str(x) + '\n')
ionsfile.close()
#for line in ions:
#	print line

"""
ionsfile_01 = open('ions_01.txt', 'w')
for i in range(len(ions)):
	for ii in range(1, len(ions[i])):
		ions[i][ii] = int(ions[i][ii])
		if ions[i][ii] != 0:
			ions[i][ii] = 1
	ionsfile_01.write(str(ions[i]) + '\n')
"""




print 'Obliczanie czestosci pojawiania sie okreslonego jonu w danej pozycji...'

#tworzenie list zawierajacych po kolei wszystkie jony w danej pozycji


positions = []
for i in range(1, len(files) + 2):
	positions.append('pos' + str(i))
#	print positions
for i in positions:
	globals()[i] = []
for i in range(len(ions)):
	for ii in range(len(positions)):
		globals()[positions[ii]].append(ions[i][ii + 1])
#for i in positions:
#        print globals()[i]

#######################################


#zliczanie ilosci donych jonow w okreslonej pozycji
print pos1
for i in positions:
        globals()['freq' + i] = {}

for i in positions:
	globals()['d' + i] = {x:globals()[i].count(x) for x in globals()[i]}
	print globals()['d' + i] 
	for ii in globals()['d' + i].keys():
		globals()['freq' + i][ii] = float(globals()['d' + i][ii]/steps)
#for i in positions:
#	print globals()['d' +i]
#for i in positions:
#        print globals()['freq' +i]


#######################################


print 'Obliczanie sredniego czasu zycia...'

def countlifetime(pos):
	lifetime = {}
	for y in range(0, len(pos)):
		if y == 0:
			lifetime[pos[0]] = []
			ltime = 0
		elif pos[y] == pos[y-1]:
			ltime += timestep
			if y == len(pos) - 1:
				ltime += timestep
				lifetime[pos[y]].append(ltime)
				break
		elif pos[y] != pos[y-1]:
			ltime += timestep
			lifetime[pos[y-1]].append(ltime)
			ltime = 0
			if not pos[y] in lifetime.keys():
				lifetime[pos[y]] = []
			if y == len(pos) - 1:
				ltime += timestep
				lifetime[pos[y]].append(ltime)
                                break
	return lifetime


def average(lifetime):
	avglt = []
	for k in sorted(lifetime.keys()):
		avg = sum(lifetime[k])/len(lifetime[k])
		ss = 0
		for s in lifetime[k]:
			ss = ss + s ** 2
		avgsq = ss/len(lifetime[k])
		var = avgsq - avg ** 2
		avglt.append([k, avg ,var])
	return avglt

for i in positions:
	globals()['lifetime' + i] = average(countlifetime(globals()[i]))
#	print globals()['lifetime' + i]


"""
#badanie korelacji

#print pos1
#print pos2
#print pos3
#print pos4

def cor(lista):
        i = 0
        c = []
        while i < len(lista):
                if lista[i] != '0':
                        ii = i
                        n = 0
                        while ii < len(lista):
                                if lista[ii] != '0':
                                        if len(c) < n + 1:
                                                c.extend('1')
                                                c[-1] = int(c[-1])
                                                n += 1
                                                ii += 1
                                        else:
                                                c[n] = c[n] + 1
                                                n += 1
                                                ii += 1
                                else:
                                        break
                i += 1
        return c
def div(c):
	if len(c) != 0:
	        d = c[0]
	        for j in range(0, len(c)):
#	                print c[j]
#	                print d
	                c[j] = float(c[j]) / float(d)
#	                print c[j]
        return c

#print cor(pos1)
#print cor(pos2)
#print cor(pos3)
#print cor(pos4)

korel1 = div(cor(pos1))
korel2 = div(cor(pos2))
korel3 = div(cor(pos3))
korel4 = div(cor(pos4))

#print korel1
#print korel2
#print korel3
#print korel4
"""

print 'Zapisywanie...'

ions_infofile = open('ions_info.txt', 'w')

ions_infofile.write('czas zajmowania przez jon okreslonego miejsca (indeks jonu: ulamek czasu):' + '\n')
for i in positions:
	ions_infofile.write(str(i) + '\n')
	for ii in sorted(globals()['freq' + i].keys()):
		ions_infofile.write(str(ii) + ' : ' + str(globals()['freq' + i][ii]) + '\n')
		
ions_infofile.write('\n')

ions_infofile.write('Sredni czas zycia jonow (indeks jonu : czas ps : wariancja):' + '\n')
for i in positions:
	ions_infofile.write(str(i) + '\n')
	for ii in globals()['lifetime' + i]:
		ions_infofile.write(str(ii[0]) + ' : ' + str(ii[1]) + ' : ' + str(ii[2]) + '\n')
	

ions_infofile.close()

ionsfile_01 = open('ions_01.txt', 'w')
for i in range(len(ions)):
        for ii in range(1, len(ions[i])):
                ions[i][ii] = int(ions[i][ii])
                if ions[i][ii] != 0:
                        ions[i][ii] = 1
        ionsfile_01.write(str(ions[i]) + '\n')



"""
tmp=open('tempnum1.txt', 'w')
j=0
for i in korel1:
	tmp.write(str(j) + ' ' + str(i) + '\n')
	j += 1
tmp.close()

tmp=open('tempnum2.txt', 'w')
j=0
for i in korel2:
        tmp.write(str(j) + ' ' + str(i) + '\n')
        j += 1
tmp.close()

tmp=open('tempnum3.txt', 'w')
j=0
for i in korel3:
        tmp.write(str(j) + ' ' + str(i) + '\n')
        j += 1
tmp.close()

tmp=open('tempnum4.txt', 'w')
j=0
for i in korel4:
        tmp.write(str(j) + ' ' + str(i) + '\n')
        j += 1
tmp.close()
"""

print 'Ukonczono z powodzeniem'